package org.example;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import static java.util.concurrent.Executors.newFixedThreadPool;

public class Executor {
    public static void main(String[] args) {

        boolean validD = false;
        boolean validT = false;

        int divisor = 1;
        int userNumber = 1;
        int threadPool;

        // Prompt the user for the maximum number between 1 and 250.  If they input text or negative number, reiterates prompt the user.
        while(!validD) {
            System.out.println("Please enter a number between 1 and 250: ");
            try {
                Scanner userThreadPool = new Scanner(System.in);
                userNumber = userThreadPool.nextInt();
                if (userNumber < 0 || userNumber > 250) {
                    System.out.println("Invalid Input. The number needs to be between 1 and 250.");
                    validD = false;
                }
                else {
                    validD = true;
                }
            } catch (InputMismatchException a) {
                System.out.println("Invalid Input. Numbers please, not text input.");
            }
        }

        // Prompt the user to give us a number for the divisor If they input text, reiterates prompt to the user.
        while(!validT) {
            System.out.println("Please enter a whole number for the divisor: ");
            try {
                Scanner userDivisor = new Scanner(System.in);
                divisor = userDivisor.nextInt();
                validT = true;
            } catch (InputMismatchException a) {
                System.out.println("Invalid Input. The divisor cannot contain text.");
            }
        }

        //Determine how many threads we will need to efficiently calculate the values.
        threadPool = calculateThreads(userNumber);

        // Call our function that will Execute the threads
        executeThreads(threadPool, divisor, userNumber);
    }

    public static int calculateThreads(int input) {
        if (input > 0 && input <= 50) {
            return 1;
        }
        else if (input > 50 && input <= 100) {
            return 2;
        }
        else if (input > 100 && input <= 150) {
            return 3;
        }
        else if (input > 150 && input <= 200) {
            return 4;
        }
        else {
            return 5;
        }
    }

    public static void executeThreads(int threads, int divisor, int userNumber) {

        // Create and Executor to run our thread(s)
        ExecutorService myService = newFixedThreadPool(threads);

        // This will execute the number of threads based on our thread-pool size.  Which was calculated in a previous function.
        switch (threads) {
            case (1) -> {
                System.out.println("- 1 thread in use... -");
                //Create instances of the Objects
                Divide p11 = new Divide(divisor, 750, 1, userNumber);
                myService.execute(p11);
            }
            case (2) -> {
                System.out.println("- 2 threads in use.. -");
                //Create instances of the Objects
                Divide p21 = new Divide(divisor, 750, 1, 50);
                Divide p22 = new Divide(divisor, 750, 50, userNumber);
                myService.execute(p21);
                myService.execute(p22);
            }
            case (3) -> {
                System.out.println("- 3 threads in use. -");
                //Create instances of the Objects
                Divide p31 = new Divide(divisor, 750, 1, 50);
                Divide p32 = new Divide(divisor, 750, 50, 100);
                Divide p33 = new Divide(divisor, 750, 100, userNumber);
                myService.execute(p31);
                myService.execute(p32);
                myService.execute(p33);
            }
            case (4) -> {
                System.out.println("- 4 threads in use! -");
                //Create instances of the Objects
                Divide p41 = new Divide(divisor, 750, 1, 50);
                Divide p42 = new Divide(divisor, 750, 50, 100);
                Divide p43 = new Divide(divisor, 750, 100, 150);
                Divide p44 = new Divide(divisor, 750, 150, userNumber);
                myService.execute(p41);
                myService.execute(p42);
                myService.execute(p43);
                myService.execute(p44);
            }
            case (5) -> {
                System.out.println("- 5 threads in use!! -");
                //Create instances of the Objects
                Divide p51 = new Divide(divisor, 750, 1, 50);
                Divide p52 = new Divide(divisor, 750, 50, 100);
                Divide p53 = new Divide(divisor, 750, 100, 150);
                Divide p54 = new Divide(divisor, 750, 150, 200);
                Divide p55 = new Divide(divisor, 750, 200, userNumber);
                myService.execute(p51);
                myService.execute(p52);
                myService.execute(p53);
                myService.execute(p54);
                myService.execute(p55);
            }
        }

        // Shutdown the Executor
        myService.shutdown();
    }
}