package org.example;


public class Divide implements Runnable {

    private final int wait;
    private final int divisor;
    private final int min;
    private final int max;

    public Divide(int divisor, int wait, int min, int max){
        this.divisor = divisor;
        this.wait = wait;
        this.min = min;
        this.max = max;
    }

    @Override
    public void run() {
        for (int count = min; count <= max; count++) {
            if (count % divisor == 0) {
                System.out.print(count + " is divisible by " + divisor +"\n");
                try {
                    Thread.sleep(wait);
                } catch (InterruptedException exploded) {
                    System.err.println();
                }
            }
        }
    }
}