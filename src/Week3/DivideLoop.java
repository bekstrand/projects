package Week3;
import java.util.InputMismatchException;
import java.util.Scanner;
public class DivideLoop {


    public static void main(String[] args) {
		int number1, number2 = 0;
		Boolean loop = true;
		float result = 0;
		while (loop) {
			try {
				System.out.print("This Program will allow you to divide two numbers.\n" + "But remember dont divide by zero or else!\n");

				Scanner input = new Scanner(System.in);

				System.out.print("Please enter the first number: \n");
				number1 = input.nextInt();

				System.out.print("Please enter a second number: \n");
				number2 = input.nextInt();

				result = theDivision(number1, number2);

				loop = false;

			} catch (InputMismatchException ime) {
				System.out.println("That number is not valid! \n");
			} catch (ArithmeticException e) {
				System.out.println("I WARNED YOU!! GAME OVER!! \n");
			} finally {
				if (loop) {
					System.out.println("Just kidding! Please try again. \n");
				} else {
					System.out.println("Here's the final result: " + result);
				}

			}
		}

	}
	public static float theDivision(int number1, int number2) throws ArithmeticException{
		float division ;
		if (number2 !=0) {
			division= (float) number1 / number2;
		}else{

			throw (new ArithmeticException());
		}
		return division;
	}
}