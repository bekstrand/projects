package Week2;

import java.util.*;


public class JavaCollections {

    public static void main(String[] args) {

              List myList = new ArrayList();
                myList.add("May");
                myList.add("the");
                myList.add("Force");
                myList.add("be");
                myList.add("with");
                myList.add("you.");

                System.out.println("~~~~~List Collection~~~~~");
                for (Object str : myList)
                System.out.println((String)str);

                Queue<Object> droid = Collections.asLifoQueue(new ArrayDeque()) ;
                droid.add("for.");
                droid.add("looking");
                droid.add("are");
                droid.add("you");
                droid.add("droids");
                droid.add("the");
                droid.add("not");
                droid.add("are");
                droid.add("These");

                System.out.println("~~~~~Queue Collection~~~~~");
                for (Object str : droid)
                System.out.println((String)str);

              Set yoda = new TreeSet();
                 yoda.add("Only");
                 yoda.add("Sith");
                 yoda.add("deal");
                 yoda.add("Absolutes");
                 yoda.add("they.");
                 yoda.add("do");

                System.out.println("~~~~~Set Collection~~~~~");
                for (Object str : yoda)
                System.out.println((String)str);

              Map hansolo = new HashMap();
                  hansolo.put(1,"Never");
                  hansolo.put(6,"Never");
                  hansolo.put(2,"tell");
                  hansolo.put(3,"me");
                  hansolo.put(5,"odds!");
                  hansolo.put(4,"the");

                System.out.println("~~~~~Map Collection~~~~~");
                for (int i =1; i < 6; i++) {
                    String ordered = (String)hansolo.get(i);

                System.out.println(ordered);
                }
                List<Gear> charItems = new LinkedList<Gear>();
                charItems.add(new Gear(" Broadsword", " Scale Mail."));
                charItems.add(new Gear(" Halberd", " Full Plate Mail."));
                charItems.add(new Gear(" Long Staff", " Robes of the Winter Night."));
                charItems.add(new Gear(" Dual Daggers", " Charcoal-Dyed Cured Leather."));
                charItems.add(new Gear(" Morning Star", " Steel Breastplate."));

                System.out.println("~~~~~Character Purchase List~~~~~");
                for (Gear equips : charItems) {
                    System.out.println(equips);
                }
    }
}
