package Week2;
//Weapon and Armor selection
public class Gear {
    private String weapon;
    private String armor;

    public Gear(String weapon, String armor) {
        this.armor = armor;
        this.weapon = weapon;
        }
        public String toString() {
            return "Weapon Choice:" + weapon + " and " + "Armor Choice:" + armor;
        }
    }

