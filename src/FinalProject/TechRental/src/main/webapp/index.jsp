<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles.css" />
    <title>Technology Gear Rental</title>
</head>
<body class="bodycfg">
<h1 class="title">Technology Gear Rental</h1>
<p class="parastyle">To view the existing rental entries, click <a href="${pageContext.request.contextPath}/Rental">here</a></p>

<p class="parastyle">To add a rental request, fill out contact information form below and click "Submit".</p>
<div class="container">
    <form action="${pageContext.request.contextPath}/Rental" method="post">
        <div class="row">
            <div class="col-25">
                <label>Full Name: </label>
            </div>
            <div class="col-75">
                <label>
                    <input type="text" name="name" placeholder="Your Name.." required>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label>Email Address: </label>
            </div>
            <div class="col-75">
                <label>
                    <input type="email" name="email" placeholder="Your Email.." required>
                </label>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-25">
                <label>Date of Birth: </label>
            </div>
            <div class="col-75">
                <label>
                    <input type="date" name="dob" placeholder="Your Date of Birth.." required>
                </label>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-25">
                <label>Phone Number (Including Area Code): </label>
            </div>
            <div class="col-75">
                <label>
                    <input type="tel" name="phone" placeholder="Your Phone Number.." required>
                </label>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-25">
                <label>Gear to Rent: </label>
            </div>
            <div class="col-75">
                <label>
                    <input type="text" name="gear" placeholder="Gear.." required>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <div class="col-25">
                    <label></label>
                </div>
                <div class="col-75">
                    <label>
                        <input name="I" type="hidden" value="--------------------------------------------------I" >
                    </label>
                </div>
            </div>
            <div class="row">
            <input type="submit" value="Submit">
        </div>
    </form>
</div>
</body>
</html>