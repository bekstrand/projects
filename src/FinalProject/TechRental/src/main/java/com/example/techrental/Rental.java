package com.example.techrental;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "Rental", value = "/Rental")
public class Rental extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        FileWriter fw = new FileWriter("GearRented.txt", true);
        BufferedWriter bw = new BufferedWriter(fw);

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body style=\"background-image: url(https://wallpaperaccess.com/full/3872532.jpg); margin-left:25%; margin-right:25%;\">");
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String dateOfBirth = request.getParameter("dob");
        String phone = request.getParameter("phone");
        String gear = request.getParameter("gear");
        String I = request.getParameter("I");

        bw.newLine();
        bw.write("Name: " + name);
        bw.newLine();
        bw.write("Email: " + email);
        bw.newLine();
        bw.write("Date of Birth: " + dateOfBirth);
        bw.newLine();
        bw.write("Phone #: " + phone);
        bw.newLine();
        bw.write("Items Rented: " + gear);
        bw.newLine();
        bw.write("I" + I);
        bw.close();

        out.println("<h1 style=\"text-align:center; color:darkred; font-weight:bold\">Submitted Items for Rent</h1>");
        for (String s : Arrays.asList("<p style=\"text-align: center; font-family:Century Gothic,serif; font-weight: bold; color:black; text-shadow: 0 0 2px darkgrey;\">Name: " + name + "</p>", "<p style=\"text-align: center; font-family:Century Gothic,serif; font-weight: bold; color:black; text-shadow: 0 0 2px darkgrey;\">Email: " + email + "</p>", "<p style=\"text-align: center; font-family:Century Gothic,serif; font-weight: bold; color:black; text-shadow: 0 0 2px darkgrey;\">Date Of Birth: " + dateOfBirth + "</p>", "<p style=\"text-align: center; font-family:Century Gothic,serif; font-weight: bold; color:black; text-shadow: 0 0 2px darkgrey;\">Phone #: " + phone + "</p>", "<p style=\"text-align: center; font-family:Century Gothic,serif; font-weight: bold; color:black; text-shadow: 0 0 2px darkgrey;\">Item Rented: " + gear + "</p>", "<p style=\"text-align:center; color:white;opacity:0;\">----------------------------------"+ I +"</p>", "</body></html>")) {
            out.println(s);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body style=\"background-image: url(https://wallpaperaccess.com/full/3872532.jpg); margin-left:25%; margin-right:25%;\">");
        out.println("<h1 style=\"text-align:center; color:darkred; font-weight:bold\">Here are the current items rented:</h1>");

        try {
            File myObj = new File("GearRented.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                out.println("<p style=\"text-align: center; font-family:Century Gothic,serif; font-weight: bold; color:black; text-shadow: 0 0 2px darkgrey;\">" + data + "</p>");
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            out.println("<p>An error occurred.</p>");
            e.printStackTrace();
        }
        out.println("</body></html>");

    }

    public void destroy() {
    }
}